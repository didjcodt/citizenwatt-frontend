if (Meteor.isClient) {

    Template.register.events({
        'submit form': function(event) {
            event.preventDefault();
            var firstName = event.target.registerFirstName.value;
            var lastName = event.target.registerLastName.value;
            var email = event.target.registerEmail.value;
            var username = event.target.registerUsername.value;
            var pass1 = event.target.registerPassword.value;
            var pass2 = event.target.registerRepeatPassword.value;

            // TODO: Check for passwords match
            if(pass1 != pass2) {;}

            Accounts.createUser({
                email: email,
                password: pass1,
                username: username,
                profile: {
                    firstName: firstName,
                    lastName: lastName
                }
            }, function (error) {
                if(error) {
                    console.log(error.reason);
                } else {
                    Router.go('/dashboard');
                }
            });
        }
    });
}