if (Meteor.isClient) {

    Meteor.subscribe("users");

    Template.dashboard.helpers({
        firstName: function() {
            return Session.get('firstName');
        },
        lastName: function() {
            return Session.get('lastName');
        },
        username: function() {
            return Session.get('username');
        },
        profileImg: function() {
            return Session.get('profileImg');
        },
        email: function() {
            return Session.get('email');
        }
    });

    Template.dashboard.created = function() {
   }

    Template.dashboard.onRendered(function() {
        if (!this.rendered) {
            this.rendered = true;
            // This code only runs once

            // Update profile
            console.log("User: " + Meteor.user());
            var profile = Meteor.user().profile;
            var firstName = profile['firstName'];
            var lastName = profile['lastName'];
            var profileImg = "/img/bg/picjumbo.com_HNCK3558.jpg";
            var email = Meteor.user().emails[0].address;
            var username = Meteor.user().username;

            Session.set('firstName', firstName);
            Session.set('lastName', lastName);
            Session.set('profileImg', profileImg);
            Session.set('email', email);
            Session.set('username', username);

            $('.js-switch').each(function(i, obj) {
                new Switchery(obj);
            });
            //$('[data-toggle="popover"]').popover();

            // Start the designer button
            $('.dash-designer-add').popover({
                html: true,
                placement: "left",
                container: "body",
                content: function() {
                    return '' +
                        '        <div>' +
                        '            <a class="btn btn-default" id="add-new-widget" href="#">Add Widget</a>' +
                        '        </div>' +
                        '       Rien à rajouter pour le moment ! Revenez plus tard !';
                }
            });
        }
    });

    Template.dashboard.events({
        'click .dashboardProfile': function() {
            console.log("Click!");
        },
        'click .dashboardLogout': function() {
            Meteor.logout();
            Session.set('firstName', null);
            Session.set('lastName', null);
            Session.set('username', null);
            Session.set('profileImg', null);
            Session.set('email', null);
            //Router.go('/login');
        }, 
        'click .navbar-expand-toggle': function (e) {
            $(".navbar-right-expand-toggle").toggleClass("fa-rotate-90");
            $(".navbar-expand-toggle").toggleClass("fa-rotate-90");
            $(".app-container").toggleClass("expanded");
        },
        /*'click .dash-designer-add': function(e) {
            $(".dash-designer-add").children().toggleClass("fa-arrow-down")
                                              .toggleClass("fadeOutDown")
                                              .toggleClass("fa-plus");
        }*/
    });
}
