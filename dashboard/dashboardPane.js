if (Meteor.isClient) {
    Template.dashboardPane.onRendered(function () {
        if (!this.rendered) {
            // Runs only on first render
            var options = {
                float: true
            };

            cfInit();
            cfPieInit();
            cfLineInit();
            cfSparkInit();
            cfGaugeInit();
            cfRAGInit();
            cfFunnelInit();
            cfSVPCInit();

            var options = {
                float: true,
                width: 50,
                resizable: {disabled: true}
            };
            $('.grid-stack').gridstack(options)

            new function () {
                this.items = [
                    {x: 0, y: 0, width: 2, height: 2},
                    {x: 3, y: 1, width: 1, height: 2},
                    {x: 4, y: 1, width: 1, height: 1},
                    {x: 2, y: 3, width: 3, height: 1},
                    {x: 2, y: 5, width: 1, height: 1}
                ];
                this.grid = $('.grid-stack').data('gridstack');
                this.add_new_widget = function () {
                    var node = this.items.pop() || {
                            x: 12 * Math.random(),
                            y: 5 * Math.random(),
                            width: 1 + 3 * Math.random(),
                            height: 1 + 3 * Math.random()
                        };
                    this.grid.add_widget($('<div><div class="grid-stack-item-content" /><div/>'),
                        node.x, node.y, node.width, node.height);
                }.bind(this);
                $('#add-new-widget').click(this.add_new_widget);
            };


            this.rendered = true;
        }

    });
}