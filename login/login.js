if (Meteor.isClient) {

    Session.set('loginBoxOpen', 'blurInv');
    Session.set('loginBoxOpenRecovery', 'hidden');

    Template.login.helpers({
        'loginBoxOpen': function () {
            return Session.get('loginBoxOpen');
        },
        'loginBoxOpenRecovery': function () {
            return Session.get('loginBoxOpenRecovery');
        }
    });

    function toggleLoginBox(strToToggle) {
        if (strToToggle == 'blurInv') {
            return 'login-switch blur';
        }
        return 'blurInv';
    }

    Template.login.events({
        // Blurry animations for password recovery
        'click .login-recover': function (e) {
            e.preventDefault();
            Session.set('loginBoxOpen', 'blur');
            Session.set('loginBoxOpenRecovery', 'blurInv');
        },
        'click .login-return': function (e) {
            e.preventDefault();
            Session.set('loginBoxOpen', 'blurInv');
            Session.set('loginBoxOpenRecovery', 'blur');
        },

        // Login form submission
        'submit form': function (e) {
            e.preventDefault();
            Meteor.loginWithPassword(e.target.loginUsername.value, e.target.loginPassword.value, function (error) {
                if (error) {
                    console.log(error.reason);
                }
                else {
                    Router.go('/dashboard');
                }

            });
        }
    });
}
