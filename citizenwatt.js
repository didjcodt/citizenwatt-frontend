Router.configure({
	layoutTemplate: 'main'
});

var OnBeforeActions;

OnBeforeActions = {
    loginRequired: function(pause) {
        if (!Meteor.userId()) {
            Router.go('/login');
            //this.render('login');
        } else {
            document.body.className = "flat-blue dashboard-page";
            this.next();
        }
    }
};

Router.onBeforeAction(OnBeforeActions.loginRequired, {
    only: ['dashboard']
});

Router.route('/', {
    onBeforeAction: function(argument) {
        if (!Meteor.userId()) {
            Router.go('/login');
        } else {
            Router.go('/dashboard');
        }
    },
    onAfterAction: function(argument) {
        //document.body.className = "flat-blue login-page";
    }
});

Router.route('/login', {
    onAfterAction: function(argument) {
        // add a class name to body
        document.body.className = "flat-blue login-page";
    }
});

Router.route('/register', {
    onAfterAction: function(argument) {
        // add a class name to body
        document.body.className = "flat-blue register-page";
    }
});

Router.route('/dashboard', {
    'preload': {
        'sync':['/js/chart.js', '/js/controlfrog-plugins.js', '/js/controlfrog.js', '/js/easypiechart.js', '/js/excanvas.min.js', '/js/gauge.js', '/js/respond.min.js']
    },
    controller: PreloadController

        /*onBeforeAction: function(argument) {
            if (Meteor.userId()) {
                this.next();
                document.body.className = "flat-blue dashboard-page";
            } else {
                this.render('login');
                document.body.className = "flat-blue login-page";
            }
        },
        onAfterAction: function(argument) {
            // add a class name to body
            //document.body.className = "flat-blue dashboard-page";
        }*/
})



if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
    Meteor.publish("users", function(){
        return Meteor.users.find({},{fields:{profile:1}})
    });
  });
}
